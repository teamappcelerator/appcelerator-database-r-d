var globals = require('globals').globalObject;
_ResetMessages();

function CreateDummyData(e) {
	//VARIABLES
	var db = require('dbManagement');
	var dbFunctions = require('databaseFunctions');
	var result = null;
	var dbStructure = null;
	var startTime = null;
	var endTime = null;
	
	//CODE
	startTime = new Date().getTime();
	$.label.text = "Start CreateDummyData";
	
	/*Get JSON Database Structure and Initiate Database*/
	globals.messageIndex++;	
	dbStructure = _SetupDbStructure();
	result = db.InitDbStructure(dbStructure, startTime);
	_PrintResult(result.messages);
	
	if(!result.success){
		return false;	
	}
	
	/*Clear Database Tables if version didn't change'*/
	if(!result.hasVersionChanged){
		result = db.ClearDbTables(startTime);
		_PrintResult(result.messages);
	}

	/*Populate Tables with dummy data*/
	result = dbFunctions.InsertDummyRecords(startTime);
	_PrintResult(result.messages);
	
	endTime = new Date().getTime();
	$.label.text += "\nEnd CreateDummyData - " + (endTime - startTime);	
	return true;
}

function _SetupDbStructure(){
	//Create JSON Object for Database Structure
	var dbStructure = {
		dbVersion:"2",//This determine if the db Design should be re-created
		dbType:'sqlite',//sqlite ; monglodb ; sculejs
		dbName:'PerformanceDb',
		tables:[
			{
				tableName:'Customer',
				columns:[
					{
						colName:'ID',
						colType:'TEXT'
					},
					{
						colName:'Name',
						colType:'TEXT'
					},
					{
						colName:'PriceList',
						colType:'TEXT'
					},
					{
						colName:'Status',
						colType:'INTEGER'
					},
					{
						colName:'LastModified',
						colType:'TEXT'
					},															
				]
			},
			{
				tableName:'Departments',
				columns:[
					{
						colName:'ID',
						colType:'TEXT'
					},
					{
						colName:'CustomerID',
						colType:'TEXT'
					},					
					{
						colName:'Name',
						colType:'TEXT'
					},
					{
						colName:'Status',
						colType:'INTEGER'
					},
					{
						colName:'LastModified',
						colType:'TEXT'
					},															
				]
			},
			{
				tableName:'Orders',
				columns:[
					{
						colName:'ID',
						colType:'TEXT'
					},
					{
						colName:'CustomerID',
						colType:'TEXT'
					},
					{
						colName:'DepartmentID',
						colType:'TEXT'
					},										
					{
						colName:'OrderDate',
						colType:'TEXT'
					},
					{
						colName:'Quantity',
						colType:'INTEGER'
					},
					{
						colName:'Price',
						colType:'INTEGER'
					},
					{
						colName:'SubTotal',
						colType:'INTEGER'
					},															
					{
						colName:'Status',
						colType:'INTEGER'
					},
					{
						colName:'LastModified',
						colType:'TEXT'
					},															
				]
			}						
		]
	};
	
	return dbStructure;
}

function _PrintResult(result){
	for(var x in result){
		if(globals.messageIndex > 0){
			$.label.text += "\n";
		}else{
			globals.messageIndex++;
		}
		
		$.label.text += result[x];
	}
	
	return true;	
}

function _ResetMessages(){
	globals.messageIndex = 0;
	$.label.text = 'Ready';
	
	return true;
}

$.index.open();
