/*
 * Using the Database Structure defined, dummy records will be created for each of the 3 tables
 * For Customers - 1000 Records = 1000
 * For Departments - 10 Records per customer = 10 000
 * For Orders - 5 Records per Department = 50 000
 */
function InsertDummyRecords(startTime){
	var currTime = new Date().getTime();
	var lastModified = new Date();
	
	var db = require('dbManagement');
	var customerTotal = 1000;
	var deptPerCustomer = 10;
	var ordersPerDept = 5;
	
	var tempCol1 = [];
	var tempCol2 = [];
	var tempCol3 = [];
	var entry = new Object();

	//Set up our JSON Object to be returned
	var result = {
		success:true,
		messages:[]
	};		
	
	result.messages.push("Start InsertDummyRecords - " + (currTime - startTime));
	
	//Set up the JSON Collection Object to be passed to the DB Management Module
	var collection = {
		tableSet:[
			{
				tableName:'Customer',
				data:[]
			},
			{
				tableName:'Departments',
				data:[]
			},
			{
				tableName:'Orders',
				data:[]
			}						
		]
	};
	
	for(var x = 0; x < customerTotal; x++){
		//Add the Customer Entry
		entry = {
			ID:x.toString(),
			Name:'Customer-' + x,
			PriceList:'Price List-' + x,
			Status:1,
			LastModified:lastModified.toDateString()
		};
		tempCol1.push(entry);
		
		//For this Customer, create Depts
		for(var y = 0; y < deptPerCustomer; y++){
			//Add the Dept Entry
			entry = {
				ID:x.toString() + y.toString(),
				CustomerID:'Customer-' + x,
				Name:'Department-' + y,
				Status:1,
				LastModified:lastModified.toDateString()
			};
			tempCol2.push(entry);
			
			//For this Dept, create Orders
			for(var z = 0; z < ordersPerDept; z++){
				//Add the Dept Entry
				entry = {
					ID:x.toString() + y.toString() + z.toString(),
					CustomerID:'Customer-' + x,
					DepartmentID:'Department-' + y,
					OrderDate:lastModified.toDateString(),
					Quantity:10,
					Price:100.00,
					SubTotal:1000.00,
					Status:1,
					LastModified:lastModified.toDateString()
				};
				tempCol3.push(entry);
			}					
		}
	}
	
	//Add Collections to JSON
	collection.tableSet[0].data = tempCol1;
	collection.tableSet[1].data = tempCol2;
	collection.tableSet[2].data = tempCol3;
	
	db.InsertDataCollection(result, startTime, collection);
	
	currTime = new Date().getTime();
	result.messages.push("End InsertDummyRecords - " + (currTime - startTime));
	return result;
	
}

exports.InsertDummyRecords = InsertDummyRecords;