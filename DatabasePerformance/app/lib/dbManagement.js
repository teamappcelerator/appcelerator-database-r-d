//CONSTANTS
var SQLITE = "sqlite";
var MONGLO_DB = "monglodb";
var SCULE_JS = "sculejs";
var VERSION_KEY = "version";

//MODULE VARIABLES
var dbStructure = null;
var defaultTable = {
	tableName:'DbPreferences',
	columns:[
		{
			colName:'Key',
			colType:'TEXT'
		},
		{
			colName:'Value',
			colType:'TEXT'
		}
	]		
};

//GLOBAL FUNCTIONS
function InitDbStructure(tempDbStructure, startTime){
	//VARIABLES
	var currTime = new Date().getTime();

	//Set up our JSON Object to be returned
	var result = {
		success:true,
		hasVersionChanged:false,
		messages:[],
	};	
	
	result.messages.push("Start InitDbStructure - " + (currTime - startTime));	
	
	//Set the Database Structure to be used for this session
	dbStructure = tempDbStructure;	
	
	//Depending on Database Type, run the necessary function
	result.messages.push("Using " + dbStructure.dbType);
	switch(dbStructure.dbType){
		case SQLITE:
			_InitSQLite(result);
			break;
		case MONGLO_DB:
		
			break;
		case SCULE_JS:
		
			break;
		default:
			result.success = false;
			result.messages.push(dbStructure.dbType + " Unsupported");
			return result;	
	}
	
	//Return JSON Object
	currTime = new Date().getTime();
	result.messages.push("End InitDbStructure - " + (currTime - startTime));	
		
	return result;
}

function ClearDbTables(startTime){
	//VARIABLES
	var currTime = new Date().getTime();
		
	//Set up our JSON Object to be returned
	var result = {
		success:true,
		messages:[],
	};
	
	result.messages.push("Start ClearDbTables - " + (currTime - startTime));
	
	result.messages.push("Dropping SQLite Tables");
	_DropSQLiteTables();
	result.messages.push("Recreating SQLite Tables");
	_CreateSQLiteTables();	
	
	currTime = new Date().getTime();
	result.messages.push("End ClearDbTables - " + (currTime - startTime));		
	return result;	
}

function InsertDataCollection(result, startTime, collection){
	var currTime = new Date().getTime();
	result.messages.push("Start InsertDataCollection - " + (currTime - startTime));
	
	//Determine the Database Type to run the relevant function
	switch(dbStructure.dbType){
		case SQLITE:
			_RunSQLiteInsert(collection, result);
			break;
		case MONGLO_DB:
			break;
		case SCULE_JS:
		
			break;
	}
	
	currTime = new Date().getTime();
	result.messages.push("End InsertDataCollection - " + (currTime - startTime));
	return true;
}

//PRIVATE FUNCTIONS
function _InitSQLite(result){
	//First we open SQLite Database. It will be created if it doesn't exist yet
	result.messages.push("Checking if Db " + dbStructure.dbName + " exists");
	var db = Ti.Database.open(dbStructure.dbName);
	var version = null;
	
	if(!_DoesDbExist(db)){
		//Database is new, so we need to create the Tables
		result.messages.push("Database is New. Need to create Tables");
		_CreateSQLiteTables(db);
	}else{
		//Database Exists, check if version has changed
		result.messages.push("Database exists. Checking version");
		version = _GetDbVersion(db);
		result.messages.push("Version is - " + version);
		db.close();
		
		if(version === null){
			result.messages.push("No Version found for Database");
			db.close();
			_DropSQLiteTables();
			_CreateSQLiteTables();
		}else{
			if(version !== dbStructure.dbVersion){
				result.hasVersionChanged = true;
				result.messages.push("Version has changed");
				result.messages.push("Dropping SQLite Tables");
				_DropSQLiteTables();
				result.messages.push("Recreating SQLite Tables");
				_CreateSQLiteTables();
			}else{
				result.messages.push("Version is the same");
			}
		}
	}

	return true;
}

function _DoesDbExist(db) {
    try{
		switch(dbStructure.dbType){
			case SQLITE:
				db.execute("SELECT * FROM " + defaultTable.tableName);
				break;	
    	}
    
		return true;
    }catch(e){
		return false;
    }
}

function _GetDbVersion(db){
	var version = null;
	
	switch(dbStructure.dbType){
		case SQLITE:
			var qry = "SELECT " + defaultTable.columns[1].colName + " FROM " + defaultTable.tableName + " WHERE " + defaultTable.columns[0].colName + " = '" + VERSION_KEY + "'";
			var result = db.execute(qry);
			
			if(result.isValidRow()){	
				version = result.fieldByName(defaultTable.columns[1].colName);
			}
			
			result.close();
			break;	
	}

	return version;	
}

function _DropSQLiteTables(db){
	var qry = "";
	var qryArray = [];
	var txt = 'DROP TABLE IF EXISTS ';
	var db = Ti.Database.open(dbStructure.dbName);
	
	//First, the default Table
	qry = txt + defaultTable.tableName;
	qryArray.push(qry);
	
	//Then, the rest of the User Defined Tables	
	for(var x in dbStructure.tables){
		qry = txt + dbStructure.tables[x].tableName;
		qryArray.push(qry);
	}
	
	db.execute('BEGIN');
	for(var x in qryArray){
		db.execute(qryArray[x]);
	}	

	db.execute('COMMIT');
	
	db.close();
	return true;		
}

function _CreateSQLiteTables(db){
	var qryArray = [];
	var qry = "";
	var db = Ti.Database.open(dbStructure.dbName);
	
	//Create Default Table and populate Version Number
	qry = _CreateTableQuery(defaultTable.tableName, defaultTable.columns);
	qryArray.push(qry);

	qry = "INSERT INTO " + defaultTable.tableName
		+ '(' + defaultTable.columns[0].colName + ', ' + defaultTable.columns[1].colName + ') VALUES("' + VERSION_KEY + '", "' + dbStructure.dbVersion + '")';
	qryArray.push(qry);
	
	//Create User Defined Tables
	for(var x in dbStructure.tables){
		qry = _CreateTableQuery(dbStructure.tables[x].tableName, dbStructure.tables[x].columns);
		qryArray.push(qry);			
	}	
	
	//Commit Queries to SQLite
	db.execute('BEGIN');
	for(var x in qryArray){
		db.execute(qryArray[x]);
	}
	db.execute('COMMIT');
	
	db.close();
	return true;	
}

function _CreateTableQuery(tableName, tableColumns){
	var qry = 'CREATE TABLE IF NOT EXISTS ' + tableName;
	qry += '(';
	
	for(var x in tableColumns){
		if(x > 0){
			qry += ', ';
		}
		
		qry += tableColumns[x].colName;
		qry += ' ' + tableColumns[x].colType;				
	}
	
	qry += ')';
	
	return qry;
}

function _RunSQLiteInsert(collection, result){
	var db = Ti.Database.open(dbStructure.dbName);
	var qry = null;
	var qryArray = [];
	var dataEntry = null;
	var table = null;
	var col = null;
	
	//Loop through affected Tables
	for(var x in collection.tableSet){
		//Get Table Layout using Db Structure
		table = null;
		
		for(var xx in dbStructure.tables){
			if(dbStructure.tables[xx].tableName === collection.tableSet[x].tableName){
				table = dbStructure.tables[xx].columns;
				break;
			}
		}
		
		if(table === null){
			result.success = false;
			result.messages.push('Table - ' + collection.tableSet[x].tableName + ' cannot be found');
			return false;
		}

		for(var y in collection.tableSet[x].data){
			dataEntry = collection.tableSet[x].data[y];

			qry = 'INSERT INTO ' + collection.tableSet[x].tableName + '(';
					
			for(var yy in table){
				col = table[yy].colName;
				
				if(typeof(col) !== 'undefined'){
					if(yy > 0){
						qry += ',';
					}
									
					qry += col;
				}
			}		
					
			qry += ') VALUES (';
			
			for(var yy in table){
				col = dataEntry[table[yy].colName];
				
				if(typeof(col) !== 'undefined'){
					if(yy > 0){
						qry += ',';
					}
						
					if(table[yy].colType !== 'INTEGER'){
						qry += '"' + col + '"';
					}else{
						qry += col;
					}			
				}
			}
						
			qry += ')';
			qryArray.push(qry);
		}	
	}
	
	//Commit Queries to SQLite
	db.execute('BEGIN');
	for(var x in qryArray){
		db.execute(qryArray[x]);
	}
	db.execute('COMMIT');

	db.close();
	return true;
}

exports.InitDbStructure = InitDbStructure;
exports.ClearDbTables = ClearDbTables;
exports.InsertDataCollection = InsertDataCollection;