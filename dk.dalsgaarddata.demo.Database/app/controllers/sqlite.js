var args = arguments[0] || {};

var doTest = function() {
    console.log("Running SQLite test...");
    var Demo = require("demo/sql");
    var demo = new Demo();
    demo.runTest();
};
