var args = arguments[0] || {};

var doTest = function() {
    console.log("Running SculeJS test...");
    var Demo = require("demo/scule");
    var demo = new Demo();
    demo.runTest();
};
