exports.definition = {
	config: {
		columns: {
		    "key":"integer",
		    "name": "text",
		    "address": "text",
		    "zip": "integer",
		    "city": "text",
		    "country": "text"
		},
		adapter: {
			type: "sql",
			collection_name: "Demo",
			idAttribute:"key"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};