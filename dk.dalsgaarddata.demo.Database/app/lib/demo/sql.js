function Demo() {
    
};
Demo.prototype.runTest = function () {
    /*
     * 2014.05.13/Jda - SQLite tests using models
     */
    
    
    Ti.API.info('-----------------------------------------------');
    Ti.API.info('       Start tests - ' + (new Date()));
    Ti.API.info('-----------------------------------------------');
    var start = (new Date()).getTime();
    Ti.API.info("Persons - create records...");
    
    var names = ['Tom', 'Dick', 'Harry', 'John'];
    for(var i=0; i < 1000; i++) {
            var person = Alloy.createModel('Demo');
            person.set('key',i);
            var name = 
            person.set('name',names[Math.floor(Math.random() * 4)]);
            person.set('address',(i*3) + ' address ' + (i*2));
            person.set('zip',Math.floor(Math.random() * (9999 - 1000 + 1) + 1000));
            person.set('city','City');
            person.set('country','Denmark');
            person.save();
    }
    
    /**
     * commits the collection to disk
     */
    var stop = (new Date()).getTime();
    Ti.API.info('Created ' + i + ' persons in ' + (stop - start) + 'ms'); 
    Ti.API.info('-----------------------------------------------');

    start = (new Date()).getTime();
    var collection = Alloy.createCollection('Demo');
    collection.fetch();
    var n = collection.length;
    stop = (new Date()).getTime();
    Ti.API.info('Fetched collection with ' + n + '  persons... in ' + (stop - start) + 'ms'); 
    Ti.API.info('-----------------------------------------------');

    start = (new Date()).getTime();
    var filter = names[0]; 
    var result = collection.where({"name":filter});
    stop = (new Date()).getTime();
    Ti.API.info('Found ' + result.length + ' persons with name ' + filter + ' in ' + (stop - start) + 'ms'); 
    Ti.API.info('-----------------------------------------------');

    start = (new Date()).getTime();
    var key = 117;
    var result = collection.where({"key":key});
    stop = (new Date()).getTime();
    Ti.API.info('Found ' + result.length + ' persons with key ' + key + ' in ' + (stop - start) + 'ms'); 
    Ti.API.info('-----------------------------------------------');
    
};
module.exports = Demo;