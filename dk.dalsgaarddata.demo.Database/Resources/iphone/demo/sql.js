function Demo() {}

Demo.prototype.runTest = function() {
    Ti.API.info("-----------------------------------------------");
    Ti.API.info("       Start tests - " + new Date());
    Ti.API.info("-----------------------------------------------");
    var start = new Date().getTime();
    Ti.API.info("Persons - create records...");
    var names = [ "Tom", "Dick", "Harry", "John" ];
    for (var i = 0; 1e3 > i; i++) {
        var person = Alloy.createModel("Demo");
        person.set("key", i);
        person.set("name", names[Math.floor(4 * Math.random())]);
        person.set("address", 3 * i + " address " + 2 * i);
        person.set("zip", Math.floor(9e3 * Math.random() + 1e3));
        person.set("city", "City");
        person.set("country", "Denmark");
        person.save();
    }
    var stop = new Date().getTime();
    Ti.API.info("Created " + i + " persons in " + (stop - start) + "ms");
    Ti.API.info("-----------------------------------------------");
    start = new Date().getTime();
    var collection = Alloy.createCollection("Demo");
    collection.fetch();
    var n = collection.length;
    stop = new Date().getTime();
    Ti.API.info("Fetched collection with " + n + "  persons... in " + (stop - start) + "ms");
    Ti.API.info("-----------------------------------------------");
    start = new Date().getTime();
    var filter = names[0];
    var result = collection.where({
        name: filter
    });
    stop = new Date().getTime();
    Ti.API.info("Found " + result.length + " persons with name " + filter + " in " + (stop - start) + "ms");
    Ti.API.info("-----------------------------------------------");
    start = new Date().getTime();
    var key = 117;
    var result = collection.where({
        key: key
    });
    stop = new Date().getTime();
    Ti.API.info("Found " + result.length + " persons with key " + key + " in " + (stop - start) + "ms");
    Ti.API.info("-----------------------------------------------");
};

module.exports = Demo;