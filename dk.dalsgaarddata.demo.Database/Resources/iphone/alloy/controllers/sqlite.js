function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "sqlite";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.sqlite = Ti.UI.createView({
        layout: "vertical",
        id: "sqlite"
    });
    $.__views.sqlite && $.addTopLevelView($.__views.sqlite);
    $.__views.__alloyId17 = Ti.UI.createButton({
        title: "Run test",
        id: "__alloyId17"
    });
    $.__views.sqlite.add($.__views.__alloyId17);
    doTest ? $.__views.__alloyId17.addEventListener("click", doTest) : __defers["$.__views.__alloyId17!click!doTest"] = true;
    var __alloyId21 = [];
    $.__views.__alloyId22 = {
        properties: {
            id: "__alloyId22"
        }
    };
    __alloyId21.push($.__views.__alloyId22);
    $.__views.__alloyId19 = Ti.UI.createListSection({
        id: "__alloyId19"
    });
    $.__views.__alloyId19.items = __alloyId21;
    var __alloyId23 = [];
    __alloyId23.push($.__views.__alloyId19);
    $.__views.__alloyId18 = Ti.UI.createListView({
        sections: __alloyId23,
        id: "__alloyId18"
    });
    $.__views.sqlite.add($.__views.__alloyId18);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var doTest = function() {
        console.log("Running SQLite test...");
        var Demo = require("demo/sql");
        var demo = new Demo();
        demo.runTest();
    };
    __defers["$.__views.__alloyId17!click!doTest"] && $.__views.__alloyId17.addEventListener("click", doTest);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;