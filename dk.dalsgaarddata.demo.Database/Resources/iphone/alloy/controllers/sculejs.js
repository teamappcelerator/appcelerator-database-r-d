function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "sculejs";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.sculejs = Ti.UI.createView({
        layout: "vertical",
        id: "sculejs"
    });
    $.__views.sculejs && $.addTopLevelView($.__views.sculejs);
    $.__views.__alloyId10 = Ti.UI.createButton({
        title: "Run test",
        id: "__alloyId10"
    });
    $.__views.sculejs.add($.__views.__alloyId10);
    doTest ? $.__views.__alloyId10.addEventListener("click", doTest) : __defers["$.__views.__alloyId10!click!doTest"] = true;
    var __alloyId14 = [];
    $.__views.__alloyId15 = {
        properties: {
            id: "__alloyId15"
        }
    };
    __alloyId14.push($.__views.__alloyId15);
    $.__views.__alloyId12 = Ti.UI.createListSection({
        id: "__alloyId12"
    });
    $.__views.__alloyId12.items = __alloyId14;
    var __alloyId16 = [];
    __alloyId16.push($.__views.__alloyId12);
    $.__views.__alloyId11 = Ti.UI.createListView({
        sections: __alloyId16,
        id: "__alloyId11"
    });
    $.__views.sculejs.add($.__views.__alloyId11);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var doTest = function() {
        console.log("Running SculeJS test...");
        var Demo = require("demo/scule");
        var demo = new Demo();
        demo.runTest();
    };
    __defers["$.__views.__alloyId10!click!doTest"] && $.__views.__alloyId10.addEventListener("click", doTest);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;