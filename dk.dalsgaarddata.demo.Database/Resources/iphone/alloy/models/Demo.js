exports.definition = {
    config: {
        columns: {
            key: "integer",
            name: "text",
            address: "text",
            zip: "integer",
            city: "text",
            country: "text"
        },
        adapter: {
            type: "sql",
            collection_name: "Demo",
            idAttribute: "key"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("Demo", exports.definition, []);

collection = Alloy.C("Demo", exports.definition, model);

exports.Model = model;

exports.Collection = collection;