function Demo() {}

Demo.prototype.runTest = function() {
    var scule = require("db/com.scule");
    scule.debug(false);
    var scollection = scule.factoryCollection("scule+titanium://demo", {
        secret: "birgitte44"
    });
    Ti.API.info("-----------------------------------------------");
    Ti.API.info("       Start tests - " + new Date());
    Ti.API.info("-----------------------------------------------");
    var timer = scule.getTimer();
    timer.startInterval("scollection - create records...");
    var names = [ "Tom", "Dick", "Harry", "John" ];
    for (var i = 0; 10001 > i; i++) {
        var a = [];
        var n = scule.global.functions.randomFromTo(3, 12);
        for (var j = 0; n > j; j++) a.push(scule.global.functions.randomFromTo(0, 100));
        var o = {
            i: i,
            n: scule.global.functions.randomFromTo(10, 30),
            s: names[scule.global.functions.randomFromTo(0, 3)],
            a: a,
            as: a.length,
            term: Math.random().toString(36).substring(7),
            ts: new Date().getTime(),
            foo: [ "bar", "bar2" ],
            o: {
                a: scule.global.functions.randomFromTo(1, 30),
                b: scule.global.functions.randomFromTo(1, 30),
                c: scule.global.functions.randomFromTo(1, 30),
                d: scule.global.functions.randomFromTo(1, 30),
                e: scule.global.functions.randomFromTo(1, 30)
            },
            loc: {
                lng: scule.global.functions.randomFromTo(-130, 130),
                lat: scule.global.functions.randomFromTo(-130, 130)
            }
        };
        scollection.save(o);
    }
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - # records: " + scollection.getLength() + ". Commit...");
    scollection.commit();
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {i:{$lte:20}, n:{$gte:6}}");
    scollection.count({
        i: {
            $lte: 20
        },
        n: {
            $gte: 6
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {i:{$in:[1, 2, 3, 4, 5]}}");
    scollection.count({
        i: {
            $in: [ 1, 2, 3, 4, 5 ]
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {'loc.lat':{$lte:5}}");
    scollection.count({
        "loc.lat": {
            $lte: 5
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {s:{$size:8}}");
    scollection.count({
        s: {
            $size: 8
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {o:{$size:5}}");
    scollection.count({
        o: {
            $size: 5
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {n:{$exists:false}}");
    scollection.count({
        n: {
            $exists: false
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {i:{$gte:70}}");
    scollection.count({
        i: {
            $gte: 70
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection -{i:{$gt:50}, $or:[{n:{$lt:40}}]}");
    scollection.count({
        i: {
            $gt: 50
        },
        $or: [ {
            n: {
                $lt: 40
            }
        } ]
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {i:{$gt:50}, $or:[{n:{$lt:40}]}, {$sort:{i:-1}, $limit:30}");
    scollection.count({
        i: {
            $gt: 50
        },
        $or: [ {
            n: {
                $lt: 40
            }
        } ]
    }, {
        $sort: {
            i: -1
        },
        $limit: 30
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {i:{$lte:90}}, {$set:{n:10, s:'Steve'}}");
    scollection.update({
        i: {
            $lte: 90
        }
    }, {
        $set: {
            n: 10,
            s: "Steve"
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {i:10}, {$push:{foo:'bar3'}}");
    scollection.update({
        i: 10
    }, {
        $push: {
            foo: "bar3"
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    timer.startInterval("scollection - {i:10}, {$pushAll:{foo:['bar3', 'bar4']}}");
    scollection.update({
        i: 10
    }, {
        $pushAll: {
            foo: [ "bar3", "bar4" ]
        }
    });
    timer.stopInterval();
    timer.logToConsole();
    timer.resetTimer();
    Ti.API.info("-----------------------------------------------");
    Ti.API.info("       Tests completed...");
    Ti.API.info("-----------------------------------------------");
};

module.exports = Demo;