function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "sculejs";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.sculejs = Ti.UI.createView({
        layout: "vertical",
        id: "sculejs"
    });
    $.__views.sculejs && $.addTopLevelView($.__views.sculejs);
    $.__views.__alloyId7 = Ti.UI.createButton({
        title: "Run test",
        id: "__alloyId7"
    });
    $.__views.sculejs.add($.__views.__alloyId7);
    doTest ? $.__views.__alloyId7.addEventListener("click", doTest) : __defers["$.__views.__alloyId7!click!doTest"] = true;
    var __alloyId11 = [];
    $.__views.__alloyId12 = {
        properties: {
            id: "__alloyId12"
        }
    };
    __alloyId11.push($.__views.__alloyId12);
    $.__views.__alloyId9 = Ti.UI.createListSection({
        id: "__alloyId9"
    });
    $.__views.__alloyId9.items = __alloyId11;
    var __alloyId13 = [];
    __alloyId13.push($.__views.__alloyId9);
    $.__views.__alloyId8 = Ti.UI.createListView({
        sections: __alloyId13,
        id: "__alloyId8"
    });
    $.__views.sculejs.add($.__views.__alloyId8);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var doTest = function() {
        console.log("Running test...");
        var Demo = require("demo/scule");
        var demo = new Demo();
        demo.runTest();
    };
    __defers["$.__views.__alloyId7!click!doTest"] && $.__views.__alloyId7.addEventListener("click", doTest);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;