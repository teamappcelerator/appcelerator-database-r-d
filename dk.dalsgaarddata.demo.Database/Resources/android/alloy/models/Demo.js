exports.definition = {
    config: {
        columns: {
            name: "string",
            address: "string",
            zip: "integer",
            city: "string",
            country: "string"
        },
        adapter: {
            type: "scule",
            collection_name: "Demo"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("Demo", exports.definition, []);

collection = Alloy.C("Demo", exports.definition, model);

exports.Model = model;

exports.Collection = collection;